#include <stdio.h>
void init(int arr[],int sz)
{
    int i = 0;
    for(i=0; i<sz; i++)
    {
        arr[i] = 0;
        printf("%d ",arr[i]);
    }
}

void print(int arr[],int sz)
{
    int i = 0;
    for(i=0; i<sz; i++)
    {
        printf("%d ",arr[i]);
    }
}

void reverse(int arr[],int sz)
{
    int left = 0;
    int right = sz-1;
    while(left<right)
    {
        int tmp = arr[left];
        arr[left] = arr[right];
        arr[right] = tmp;
        left++;
        right--;
    }
}

int main()
{
    int arr[10];
    int sz = sizeof(arr)/sizeof(arr[0]);
    init(arr,sz);
    print(arr,sz);
    reverse(arr,sz);
    return 0;
}