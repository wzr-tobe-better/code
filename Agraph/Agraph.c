// #include <stdio.h>
// #include <stdlib.h>

// #define MAX_VERTEX_NUM 50  

// typedef struct arcnode {
//     int adjvex;    
//     struct arcnode* nextarc; 
// } arcnode; 

// typedef struct vexnode {
//     arcnode* firstarc;  
//     int tag;   
// } vexnode;  

// typedef struct {
//     vexnode adjlist[MAX_VERTEX_NUM];  
//     int vexnum, arcnum;  
// } Agraph;   


// Agraph* createGraph(int numVertices) {
//     Agraph* graph = (Agraph*)malloc(sizeof(Agraph));
//     graph->vexnum = numVertices;
//     graph->arcnum = 0;

//     for (int i = 0; i < numVertices; i++) {
//         graph->adjlist[i].firstarc = NULL;
//         graph->adjlist[i].tag = 0;
//     }

//     return graph;
// }

// void addEdge(Agraph* graph, int src, int dest) {
//     arcnode* newNode = (arcnode*)malloc(sizeof(arcnode));
//     newNode->adjvex = dest;
//     newNode->nextarc = graph->adjlist[src].firstarc;
//     graph->adjlist[src].firstarc = newNode;
//     newNode = (arcnode*)malloc(sizeof(arcnode));
//     newNode->adjvex = src;
//     newNode->nextarc = graph->adjlist[dest].firstarc;
//     graph->adjlist[dest].firstarc = newNode;

//     graph->arcnum++;
// }

// void DFS(Agraph* graph, int vertex) {
//     graph->adjlist[vertex].tag = 1;
//     printf("%d ", vertex);
//     arcnode* temp = graph->adjlist[vertex].firstarc;
//     while (temp != NULL) {
//         int adjVertex = temp->adjvex;
//         if (!graph->adjlist[adjVertex].tag)
//             DFS(graph, adjVertex);
//         temp = temp->nextarc;
//     }
// }

// void BFS(Agraph* graph, int startVertex) {
//     struct Queue {
//         int front, rear;
//         int capacity;
//         int* array;
//     };

//     struct Queue* queue = (struct Queue*)malloc(sizeof(struct Queue));
//     queue->capacity = graph->vexnum;
//     queue->front = -1;
//     queue->rear = -1;
//     queue->array = (int*)malloc(queue->capacity * sizeof(int));

//     queue->array[++queue->rear] = startVertex;
//     graph->adjlist[startVertex].tag = 1;

//     while (queue->front < queue->rear) {
//         int vertex = queue->array[++queue->front];
//         printf("%d ", vertex);

//         arcnode* temp = graph->adjlist[vertex].firstarc;
//         while (temp != NULL) {
//             int adjVertex = temp->adjvex;
//             if (!graph->adjlist[adjVertex].tag) {
//                 queue->array[++queue->rear] = adjVertex;
//                 graph->adjlist[adjVertex].tag = 1;
//             }
//             temp = temp->nextarc;
//         }
//     }
//     free(queue->array);
//     free(queue);
// }

// int main() {
//     Agraph* graph = createGraph(6);
//     addEdge(graph, 0, 1);
//     addEdge(graph, 0, 2);
//     addEdge(graph, 1, 3);
//     addEdge(graph, 2, 3);
//     addEdge(graph, 2, 4);
//     addEdge(graph, 3, 4);
//     addEdge(graph, 3, 5);

//     printf("深度优先搜索遍历结果：");
//     DFS(graph, 0);
//     printf("\n");


//     for (int i = 0; i < graph->vexnum; i++) {
//         graph->adjlist[i].tag = 0;
//     }
//     printf("广度优先搜索遍历结果：");
//     BFS(graph, 0);
//     printf("\n");

//     return 0;
// }

// #include <stdio.h>
// #include <stdlib.h>
// #include <stdbool.h>
// #include <limits.h>

// #define MAX_VERTICES 100

// typedef struct {
//     int numVertices;
//     int adjacencyMatrix[MAX_VERTICES][MAX_VERTICES];
// } Graph;

// Graph* createGraph(int numVertices) {
//     Graph* graph = (Graph*)malloc(sizeof(Graph));
//     graph->numVertices = numVertices;
//     for (int i = 0; i < numVertices; i++) {
//         for (int j = 0; j < numVertices; j++) {
//             graph->adjacencyMatrix[i][j] = 0;
//         }
//     }

//     return graph;
// }
// void addEdge(Graph* graph, int src, int dest, int weight) {
//     graph->adjacencyMatrix[src][dest] = weight;
//     graph->adjacencyMatrix[dest][src] = weight;
// }

// int findMinKey(int key[], bool mstSet[], int numVertices) {
//     int minKey = INT_MAX;
//     int minIndex;

//     for (int v = 0; v < numVertices; v++) {
//         if (!mstSet[v] && key[v] < minKey) {
//             minKey = key[v];
//             minIndex = v;
//         }
//     }

//     return minIndex;
// }

// // 构造最小生成树
// void primMST(Graph* graph) {
//     int parent[MAX_VERTICES];
//     int key[MAX_VERTICES];
//     bool mstSet[MAX_VERTICES];


//     for (int v = 0; v < graph->numVertices; v++) {
//         key[v] = INT_MAX;
//         mstSet[v] = false;
//     }

//     key[0] = 0;
//     parent[0] = -1; 

//     for (int count = 0; count < graph->numVertices - 1; count++) {
//         int u = findMinKey(key, mstSet, graph->numVertices);
//         mstSet[u] = true;

//         for (int v = 0; v < graph->numVertices; v++) {
//             if (graph->adjacencyMatrix[u][v] && !mstSet[v] && graph->adjacencyMatrix[u][v] < key[v]) {
//                 parent[v] = u;
//                 key[v] = graph->adjacencyMatrix[u][v];
//             }
//         }
//     }

//     printf("最小生成树的边:\n");
//     for (int i = 1; i < graph->numVertices; i++) {
//         printf("%d - %d\n", parent[i], i);
//     }
// }

// int main() {
//     int numVertices = 6;
//     Graph* graph = createGraph(numVertices);

//     addEdge(graph, 0, 1, 4);
//     addEdge(graph, 0, 2, 2);
//     addEdge(graph, 1, 2, 1);
//     addEdge(graph, 1, 3, 5);
//     addEdge(graph, 2, 3, 8);
//     addEdge(graph, 2, 4, 10);
//     addEdge(graph, 3, 4, 2);
//     addEdge(graph, 3, 5, 6);
//     addEdge(graph, 4, 5, 3);

//     primMST(graph);

//     return 0;
// }
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define MAX_LOCATIONS 100
typedef struct {
    char name[50];
} Location;

typedef struct {
    int numLocations;
    int adjacencyMatrix[MAX_LOCATIONS][MAX_LOCATIONS];
    Location locations[MAX_LOCATIONS];
} CampusMap;

CampusMap* createMap() {
    CampusMap* map = (CampusMap*)malloc(sizeof(CampusMap));
    map->numLocations = 0;

    for (int i = 0; i < MAX_LOCATIONS; i++) {
        for (int j = 0; j < MAX_LOCATIONS; j++) {
            map->adjacencyMatrix[i][j] = 0;
        }
    }

    return map;
}

void addLocation(CampusMap* map, const char* name) {
    if (map->numLocations >= MAX_LOCATIONS) {
        printf("地点数量已达到上限，无法添加新地点。\n");
        return;
    }

    Location location;
    strcpy(location.name, name);
    map->locations[map->numLocations++] = location;
}

void addConnection(CampusMap* map, int location1, int location2) {
    if (location1 < 0 || location1 >= map->numLocations || location2 < 0 || location2 >= map->numLocations) {
        printf("无效的地点索引。\n");
        return;
    }

    map->adjacencyMatrix[location1][location2] = 1;
    map->adjacencyMatrix[location2][location1] = 1;
}

void printMap(CampusMap* map) {
    printf("校园地图:\n");
    for (int i = 0; i < map->numLocations; i++) {
        printf("%d. %s\n", i, map->locations[i].name);
    }
}

void DFS(CampusMap* map, int startLocation, bool visited[]) {
    visited[startLocation] = true;
    printf("%s ", map->locations[startLocation].name);

    for (int i = 0; i < map->numLocations; i++) {
        if (map->adjacencyMatrix[startLocation][i] && !visited[i]) {
            DFS(map, i, visited);
        }
    }
}
void campusGuide(CampusMap* map, int startLocation) {
    bool visited[MAX_LOCATIONS] = { false };

    printf("从%s出发进行导游:\n", map->locations[startLocation].name);
    DFS(map, startLocation, visited);
    printf("\n");
}

int main() {
    CampusMap* map = createMap();
    addLocation(map, "图书馆");
    addLocation(map, "教学楼A");
    addLocation(map, "教学楼B");
    addLocation(map, "食堂");
    addLocation(map, "操场");
    addLocation(map, "实验室");
    addConnection(map, 0, 1);
    addConnection(map, 0, 2);
    addConnection(map, 1, 3);
    addConnection(map, 2, 3);
    addConnection(map, 2, 4);
    addConnection(map, 3, 5);
    printMap(map);
    campusGuide(map, 0);

    free(map);

    return 0;
}
