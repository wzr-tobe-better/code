#include <stdio.h>
int main()
{
    int arr1[] = {1,3,5,7,9};
    int arr2[] = {2,4,6,8,0};
    int temp = 0;
    int sz = sizeof(arr1)/sizeof(arr1[0]);
    int i = 0;
    for(i=0; i<sz; i++)
    {
        int temp = arr1[i];
        arr1[i] = arr2[i];
        arr2[i] = temp;
    }
    return 0;
}