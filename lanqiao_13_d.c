#include<stdio.h>
int main() 
{
    int n = 0;
    int i = 0;
    scanf("%d", &n);
    for (i = 1; i <= n / 2; i++)
    {
        printf("%d\n", 2 * n - 2 * i);
    }
    if (n % 2 != 0) 
    {
        printf("%d\n", n - 1);
    }
    for (i = n / 2; i > 0; i--) 
    {
        printf("%d\n", 2 * n - 2 * i);
    }
    return 0;
}