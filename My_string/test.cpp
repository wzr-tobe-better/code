#include "Mystring.h"

void test_string1()
{
    w ::string s1("hello world");
    cout << s1.c_str() << endl;

    for (size_t i = 0; i < s1.size(); i++)
    {
        cout << s1[i] << " ";
    }
    cout << endl;

    w ::string::iterator it = s1.begin();
    while (it != s1.end())
    {
        cout << *it << " ";
        ++it;
    }
    cout <<endl;
    
    for (auto ch : s1)
    {
        cout << ch <<" ";
    }
    cout <<endl;
}

void test_string2()
{

	w::string s1("hello world");
	cout << s1.c_str() << endl;

	s1.push_back(' ');
	s1.push_back('#');
	s1.append("hello");
	cout << s1.c_str() << endl;

    w::string s2("hello world");
	cout << s2.c_str() << endl;

	s2 += ' ';
	s2 += '#';
	s2 += "hello code";
	cout << s2.c_str() << endl;

}

void test_string3()
{
	w::string s1("helloworld");
	cout << s1.c_str() << endl;

	s1.insert(5, 3, '#');
	cout << s1.c_str() << endl;

	s1.insert(0, 3, '#');
	cout << s1.c_str() << endl;

    w::string s2("helloworld");
	s2.insert(5, "%%%%%");
	cout << s2.c_str() << endl;
	
}

void test_string4()
{
	w::string s1("helloworld");
	cout << s1.c_str() << endl;

	s1.erase(5, 3);
	cout << s1.c_str() << endl;

	s1.erase(5, 30);
	cout << s1.c_str() << endl;

	s1.erase(2);
	cout << s1.c_str() << endl;
}

void test_string5()
{
	w::string s1("helloworld");
	cout << s1.find('w',2) << endl;

	
}

void test_string6()
{
	w::string s1("hello world");
	w::string s2(s1);

	cout << s1.c_str() << endl;
	cout << s2.c_str() << endl;

}




int main()
{
    test_string6();
    return 0;
}