#include <stdio.h>
#define  N  8
struct student
{
char num[10];
float a[N];
float ave;
};
void fun(struct student *s)
{
	float x;
	for(int i=0;i<8;i++)
	{x+=s->a[i];
	}
	s->ave=x/8;
}
int main()
{
struct student s={"007",85.5,76,69.5,85,91,72,64.5,87.5};
int i;
fun(&s);
printf("The %s's student data:\n",s.num);
for(i=0;i<N;i++)
printf("%4.1f\n",s.a[i]);
printf("ave=%7.2f\n",s.ave);
return 0;
 }
