#include "Date.h"

void TestDate1()
{
	Date d1(2023, 2, 4);
	d1.Print();

	/*Date d2(2023, 2, 29);
	d2.Print();*/

	Date d2 = d1 + 5000;
	d2.Print();
	d1.Print();

	Date d3 = d1;
	//d1 = d3 += 100;
	d3 += 100;

	d3.Print();
	d1.Print();

	int i = 0 , j = 0, k = 1;
	i += j += k;
}

void TestDate2()
{
	Date d1(2023, 2, 4);
	d1.Print();

	Date d2 = d1 + 100;
	d2.Print();

	Date d3 = d1 + 100;
	d3.Print();
}

void TestDate3()
{
	Date d1(2023, 2, 4);
	d1.Print();

	Date ret1 = ++d1;  // d1.operator++();
	d1.Print();
	ret1.Print();


	Date ret2 = d1++;  // d1.operator++(0);
	d1.Print();
	ret2.Print();

	--d1;
	d1--;

	/*d1.operator++();
	d1.operator++(0);*/
}

void TestDate4()
{
	Date d1(2023, 2, 4);
	d1.Print();

	d1 -= 100;
	d1.Print();

	Date d2(2023, 2, 7);
	d2.Print();
	d2 += -100;
	d2.Print();

	Date d3(2023, 2, 7);
	d3.Print();
	d3 -= -200;
	d3.Print();
}

void TestDate5()
{
	Date d1(2023, 2, 7);
	d1.Print();

	Date d2(1368, 1, 1);
	d2.Print();

	cout << d2 - d1 << endl;
	cout << d1 - d2 << endl;
}

//void operator<<(ostream& out);
// ostream& operator<<(ostream& out, const Date& d)

//ostream& operator<<(ostream& out, const Date& d)
//{
//	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
//	return out;
//}


void TestDate6()
{
	// 流插入
	Date d1(2023, 2, 4);
	Date d2(2022, 1, 1);
	//cout << d1;
	//d1.operator<<(cout);
	//d1 << cout;

	operator<<(cout, d1);
	cout << d1;
	cout << d1 << d2 << endl;

	d1 -= 100;
	//cout << d1;

	int i = 1;
	double d = 1.11;

	// 运算符重载+函数重载
	cout << i; // cout.operator<<(i) // int
	cout << d; // cout.operator<<(d) // double
}

void TestDate7()
{
	Date d1;
	cin >> d1;
	cout << d1;
}

//int main()
//{
//	TestDate7();
//
//	return 0;
//}

//class A
//{
//public:
//	// const 修饰 *this
//	// this的类型变成 const A*
//	// 内部不改变成员变量的成员函数
//	// 最好加上const，const对象和普通对象都可以调用
//	void Print() const
//	{
//		cout << _a << endl;
//	}
//
//	void Add(int x)
//	{
//		_a += x;
//	}
//
//	A* operator&()
//	{
//		//return this;
//		return (A*)100000;
//	}
//
//	const A* operator&() const
//	{
//		//return this;
//		return nullptr;
//	}
//
//private:
//	int _a = 10;
//};
//
//void Func(const A& x)
//{
//	x.Print();
//
//	cout << &x << endl;
//}
//
//int main()
//{
//	A aa;
//	aa.Print();
//
//	A bb;
//	bb.Print();
//
//	Func(aa);
//
//	cout << &aa << endl;
//	cout << &bb << endl;
//
//	return 0;
//}


//class Array
//{
//public:
//	int& operator[](int i)
//	{
//		assert(i < 10);
//
//		return _a[i];
//	}
//
//	const int& operator[](int i) const
//	{
//		assert(i < 10);
//
//		return _a[i];
//	}
//private:
//	int _a[10];
//	int _size;
//};
//
//void Func(const Array& aa)
//{
//	for (int i = 0; i < 10; ++i)
//	{
//		//aa[i]++;
//		cout << aa[i] << " ";
//	}
//}
//
//int main()
//{
//	Array a;
//	//for (int i = 0; i < 10; ++i)
//	//{
//	//	a[i] = i;
//	//}
//
//	for (int i = 0; i < 10; ++i)
//	{
//		cout << a[i] << " ";
//	}
//	cout << endl;
//
//	Func(a);
//
//	return 0;
//}

//class B
//{
//public:
//	B(int b)
//		:_b(0)
//	{
//		cout << "B()" << endl;
//	}
//private:
//	int _b;
//};
//
//class A
//{
//public:
//
//	// 1、哪个对象调用构造件数，初始化列表是它所有成员变量定义的位置
//	// 2、不管是否显示在初始化列表写，那么编译器每个变量都会初始化列表定义初始化
//	A()
//		:_x(1)
//		, _a2(1)
//		, _ref(_a1)
//		, _bb(0)
//	{
//		_a1++;
//		_a2--;
//	}
//
//private:
//	int _a1 = 1; // 声明
//	int _a2 = 2;
//
//	// 
//	const int _x;
//	int& _ref;
//	B _bb;
//};