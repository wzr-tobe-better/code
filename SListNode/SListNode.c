#include "SListNode.h"
 SLTNode* BuySListNode(SLTDateType x)
{
    SLTNode* newnode = (SLTNode*) malloc(sizeof(SLTNode));
    if (newnode == NULL)
    {
        perror("malloc fail");
        exit(-1);
    }

    newnode -> data = x;
    newnode -> next = NULL;

    return newnode; 

}

//生成单链表
SLTNode* createSLNode(int n)
{
    SLTNode* phead = NULL;
    SLTNode* ptail = NULL;
    for (size_t i = 0; i < n; ++i)
    {
        SLTNode* newnode = BuySListNode(i);
        if (phead == NULL)
        {
            phead = ptail = newnode;
        }
        else
        {
            ptail -> next = newnode;
            ptail = ptail -> next;
        }
        
    }

    return phead;
    
}


//单链表打印
void SLTPrint(SLTNode* phead)
{
    SLTNode* cur = phead;
    while(cur)
    {
        printf("%d->",cur ->data);
        cur = cur -> next;
    }
    printf("NULL\n");
}

//单链表尾插
void SLTPushBack(SLTNode** pphead, SLTDateType x)
{
    SLTNode* newnode = BuySListNode(x);
    SLTNode* tail = *pphead;
    if (*pphead == NULL)
    {
        *pphead = newnode;
    }
    else
    {
        while (tail -> next)
        {
            tail = tail -> next;
        }

        tail -> next = newnode;
    }
     
}

//单链表尾删
void SLTPopBack(SLTNode** pphead)
{
    assert(*pphead);
    SLTNode* tail = *pphead;
    SLTNode* prev = NULL;
    if ((*pphead)  -> next == NULL)
    {
        free(*pphead);
        *pphead = NULL;
    }
    else
    {
         while (tail -> next)
        {
            prev = tail;
            tail = tail -> next;
        }
    
        free(tail);
        prev -> next = NULL;

    }
    
}

//单链表头插
void SLTPushFornt(SLTNode** pphead, SLTDateType x)
{
    SLTNode* newnode = BuySListNode(x);
    newnode -> next = *pphead;
    *pphead = newnode;
    
}

//单链表头删
void SLTPopFornt(SLTNode** pphead)
{
    assert(*pphead);
    SLTNode* next = (*pphead) -> next;
    free(*pphead);
    *pphead = next;
    
}

//单链表查找
SLTNode* SLTFind(SLTNode* phead, SLTDateType x)
{
    SLTNode* cur = phead;
    while (cur)
    {
        if (cur -> data == x)
        {
            return cur;
        }
        cur = cur -> next;
        
    }
    
    return NULL;
    
}

int SLTFindNum(SLTNode* phead,SLTDateType x)
{
    int num = 0;
    SLTNode* cur = phead;
    while (cur)
    {
        if (cur -> data == x)
        {
            num++;
        }
        cur = cur -> next;
        
    }
    
    return num;
}

//在pos位置之后插入
void SLTInsertAfter(SLTNode* pos, SLTDateType x)
{
    assert(pos);
    SLTNode* newnode = BuySListNode(x);
    SLTNode* next = pos -> next;
    pos -> next = newnode;
    newnode -> next = next;
}

//在pos位置之前插入
void SLTInsert(SLTNode** pphead, SLTNode* pos, SLTDateType x)
{
    assert(pos);
    if (pos == *pphead)
    {
        SLTPushFornt(pphead, x);
    }
    else
    {
        SLTNode* prev = *pphead;
        SLTNode* newnode = BuySListNode(x);
        while(prev -> next != pos)
        {
            prev = prev -> next;
        }

        prev -> next = newnode;
        newnode -> next = pos;
        
    }
        
}

//删除pos后一个位置
void SLTEraseAfter(SLTNode* pos)
{
    assert(pos);
    if (pos -> next == NULL)
    {
        return;
    }
    else
    {
        SLTNode* nextnode = pos -> next;
        pos -> next = nextnode -> next;
        free(nextnode);
    }
    
}

//在pos位置删除
void SLTErase(SLTNode** pphead, SLTNode* pos)
{
    assert(pos);
    if (pos == *pphead )
    {
        SLTPopFornt(pphead);
    }
    else
    {
        SLTNode* prev = *pphead;
        while (prev -> next != pos)
        {
            prev = prev -> next;
        }
        prev -> next = pos -> next;
        free(pos);
        
    }
    
}

//单链表删除
void SLTDestroy(SLTNode** pphead)
{
    SLTNode* cur = *pphead;
    while(cur)
    {
        SLTNode* next = cur -> next;
        free(cur);
        cur = next;
    }
    
    *pphead = NULL;

}










