#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
typedef int SLTDateType;
typedef struct SListNode
{
    SLTDateType data;
    struct SListNode* next;
}SLTNode;
SLTNode* BuySListNode(SLTDateType x);
SLTNode* createSLNode(int n);
void SLTPrint(SLTNode* phead);
void SLTPushBack(SLTNode** pphead, SLTDateType x);
void SLTPushFornt(SLTNode** pphead, SLTDateType x);
void SLTPopBack(SLTNode** pphead);
void SLTPopFornt(SLTNode** pphead);
SLTNode* SLTFind(SLTNode* phead, SLTDateType x);
int SLTFindNum(SLTNode* phead,SLTDateType x);
void SLTInsertAfter(SLTNode* pos, SLTDateType x);
void SLTInsert(SLTNode** pphead, SLTNode* pos, SLTDateType x);
void SLTEraseAfter(SLTNode* pos);
void SLTErase(SLTNode** pphead, SLTNode* pos);
void SLTDestroy(SLTNode** pphead);
