#include <stdio.h>
#define N 12 
typedef struct 
{ 	char num[10]; 
	double s; 
} STREC; 
double fun( STREC *a, STREC *b, int *n ) 
{ 	 
	int i;
    double aver=0;
    *n=0;
    for(i=0;i<N;i++)
        aver+=a[i].s/N;
    for(i=0;i<N;i++)
        if(a[i].s>=aver)
            b[(*n)++]=a[i];
    return aver;
} 
int main() 
{ STREC s[N]={{"GA05",85},{"GA03",76}, {"GA02",69}, {"GA04",85},{"GA01",91},{"GA07",72}, {"GA08",64},{"GA06",87},{"GA09",60},{"GA11",79},{"GA12",73}, {"GA10",90}}; 
	STREC h[N], t;
	int i,j,n; double ave; 
	ave=fun( s,h,&n ); 
	printf("The %d student data which is higher than %7.3f:\n",n,ave); 
	for(i=0;i<n; i++) 
		printf("%s %4.1f\n",h[i].num,h[i].s); 
	printf("\n"); 
}
