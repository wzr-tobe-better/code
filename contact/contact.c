#include "contact.h"
// 静态版本
// void InitContact(struct Contact* pc)
// {
//     assert(pc);

//     pc->sz = 0;
//     memset(pc->date,0,DATE_MAX*sizeof(struct PeoInfo));
// }
int check_capacity(struct Contact* pc);

void LoadContact(struct Contact* pc)
{
    FILE* pfR = fopen("data.txt","rb");
    if(pfR == NULL)
    {
        perror("LoadContact::fopen");
        return;
    }

    struct PeoInfo tmp = {0};

    while (fread(&tmp ,sizeof( struct PeoInfo) ,1 ,pfR))
    {
        check_capacity(pc);
        pc->date[pc->sz] = tmp;
        pc->sz++;

    }

    fclose(pfR);
    pfR = NULL;
}




void InitContact(struct Contact* pc)
{
    assert(pc);
    
    pc->date = (struct PeoInfo*)malloc(DEFAULT_SZ * sizeof(struct PeoInfo));
    if(pc->date == NULL)
    {
        perror("InitContact()");
        return;
    }
    pc->sz = 0;
    pc->capacity = DEFAULT_SZ;
    //加载文件中的信息，到通讯录中
    LoadContact(pc);
}

void DestroyContact(struct Contact* pc)
{
    free(pc->date);
    pc->date = NULL;
    pc->capacity = 0;
    pc->sz = 0;
}

// 静态版本
// void AddContact(struct Contact* pc)
// {
//     assert(pc);

//     if (pc->sz == DATE_MAX)
//     {
//         printf("通讯录已满，无法添加数据\n");
//         return;
//     }


int  check_capacity(struct Contact* pc)
{
    if (pc->sz == pc->capacity)
    {
        struct PeoInfo* ptr = (struct PeoInfo*)realloc(pc->date,(pc->capacity + INT_SZ)*sizeof(struct PeoInfo));
        if(ptr != NULL)
        {
            pc->date = ptr;
            pc->capacity += INT_SZ;
            printf("扩容成功");
            return 1;

        }
        else
        {
            perror("AddContact()");
            return 0;
        }
    }
    else
    {
        return 1;
    }

}
void AddContact(struct Contact* pc)
{
    assert(pc);

    check_capacity(pc);
    if (0 == check_capacity(pc))
    {
        return;
    }
    
    //增加人的信息
    printf("请输入名字：>");
    scanf("%s",pc->date[pc->sz].name);
    printf("请输入性别：>");
    scanf("%s",pc->date[pc->sz].sex);
    printf("请输入年龄：>");
    scanf("%d",&(pc->date[pc->sz].age));
    printf("请输入电话：>");
    scanf("%s",pc->date[pc->sz].tele);
    printf("请输入地址：>");
    scanf("%s",pc->date[pc->sz].addr);

    pc->sz++;
    printf("成功增加联系人\n");
    
}

void ShowContact(const struct Contact* pc)
{
    int i = 0;
    printf("%-20s\t%-5s\t%-5s\t%-12s\t%-30s\n","名字","性别","年龄","电话","地址");
    for(i = 0; i < pc->sz; ++i)
    {
        printf("%-20s\t%-5s\t%-5d\t%-12s\t%-30s\n",pc->date[i].name,
        pc->date[i].sex,
        pc->date[i].age,
        pc->date[i].tele,
        pc->date[i].addr);
        
    }
}

static int FindByName(const struct Contact* pc,char name[])
{
    int i = 0;
    for(i = 0; i < pc->sz; ++i)
    {
        if(0 == strcmp(pc->date[i].name,name))
        {
            return i;
        }
    }
    return -1;
}

void DelContact(struct Contact* pc)
{
    char name[NAME_MAX];
    printf("请输入要删除人的名字：>");
    scanf("%s",name);
    // 查找一下指定联系人是否存在
    int ret = FindByName(pc,name);
    if(-1 == ret)
    {
        printf("要删除的联系人不存在\n");
    }
    else
    {
        int j = 0;
        for(j = ret; j < pc->sz - 1; ++j)
        {
            pc->date[j] = pc->date[j+1];
        }
        pc->sz--;
        printf("成功删除指定联系人");
    }

}

void SearchContact(const struct Contact* pc)
{
     char name[NAME_MAX];
    printf("请输入要查找人的名字：>");
    scanf("%s",name);
    // 查找一下指定联系人是否存在
    int ret = FindByName(pc,name);
    if(-1 == ret)
    {
        printf("要查找的联系人不存在\n");
    }
    else
    {
        printf("%-20s\t%-5s\t%-5s\t%-12s\t%-30s\n","名字","性别","年龄","电话","地址");
        printf("%-20s\t%-5s\t%-5d\t%-12s\t%-30s\n",pc->date[ret].name,
            pc->date[ret].sex,
            pc->date[ret].age,
            pc->date[ret].tele,
            pc->date[ret].addr);
    }
}

void ModifyContact(struct Contact* pc)
{
    char name[NAME_MAX];
    printf("请输入要修改人的名字：>");
    scanf("%s",name);
    // 查找一下指定联系人是否存在
    int ret = FindByName(pc,name);
    if(-1 == ret)
    {
        printf("要修改的联系人不存在\n");
    }
    else
    {
        printf("请输入名字：>");
        scanf("%s",pc->date[pc->sz].name);
        printf("请输入性别：>");
        scanf("%s",pc->date[pc->sz].sex);
        printf("请输入年龄：>");
        scanf("%d",&(pc->date[pc->sz].age));
        printf("请输入电话：>");
        scanf("%s",pc->date[pc->sz].tele);
        printf("请输入地址：>");
        scanf("%s",pc->date[pc->sz].addr);
        printf("修改成功\n");
    }
}

int CmpByAge(const void* e1,const void* e2)
{
    return((struct PeoInfo*)e1)->age - ((struct PeoInfo*)e2)->age;
}

void SortContact(struct Contact* pc)
{
    qsort(pc->date,pc->sz,sizeof(struct PeoInfo),CmpByAge);
}

void SaveContact(struct Contact* pc)
{
    FILE*pfw = fopen("data.txt","wb");
    if(pfw == NULL)
    {
        perror("SaveContact::fopen");
        return ;
    }
    int i = 0;
    for (int i = 0; i < pc->sz; i++)
    {
        fwrite(pc->date+i, sizeof(struct PeoInfo), 1, pfw);

    }

    fclose(pfw);
    pfw = NULL;
    
}




