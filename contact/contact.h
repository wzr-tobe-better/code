#define NAME_MAX 20
#define SEX_MAX 5
#define TELE_MAX 12
#define ADDR_MAX 30
#define DATE_MAX 100
//通讯录默认大小
#define DEFAULT_SZ 3
//空间满后每次增加两个
#define INT_SZ 2
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
struct PeoInfo
{
    char name[NAME_MAX];
    char sex[SEX_MAX];
    char tele[TELE_MAX];
    int age;
    char addr[ADDR_MAX];
};
struct Contact
{
    struct PeoInfo* date;
    int sz;
    int capacity;
};
void InitContact(struct Contact* pc);
void DestroyContact(struct Contact* pc);
void AddContact(struct Contact* pc);
void ShowContact(const struct Contact* pc);
void DelContact(struct Contact* pc);
void SearchContact(const struct Contact* pc);
void ModifyContact(struct Contact* pc);
void SortContact(struct Contact* pc);
void SaveContact(struct Contact* pc);
void LoadContact(struct Contact* pc);








