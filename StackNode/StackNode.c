typedef int ElemType;
typedef struct StackNode
{
    ElemType data;
    struct StackNode* next;
}STNode;
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
void STNodeInit(STNode* ps)
{
    ps = NULL;
}

void STNodePush(STNode* ps, ElemType x)
{
    STNode* p = (STNode*)malloc(sizeof(ElemType));
    p -> data = x;
    p -> next = ps;
    ps = p;
}

void STNodePop(STNode* ps, ElemType x)
{
    if (ps == NULL)
    {
        perror("ps = NULL");
        exit(-1);
    }
    x = ps -> data;
    STNode* p = (STNode*)malloc(sizeof(ElemType));
    p = ps;
    ps = ps -> next;
    free(p);
}

ElemType GetTop(STNode* ps)
{
    if(ps != NULL)
    {
        return ps ->data;
    }
}
void STNodetest()
{
    STNode* s;
    STNodeInit(s);
    STNodePush(s,1);
    STNodePush(s,2);
    STNodePush(s,3);
    STNodePush(s,4);
    STNodePush(s,5);
    printf("%d\n",GetTop(s));
    STNodePop(s,5);
    STNodePop(s,4);
    STNodePop(s,3);
    printf("%d\n",GetTop(s));
}
int main()
{
    STNodetest();
}
