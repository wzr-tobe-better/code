#include"Stack.h"
void StackInit(ST* ps)
{
    assert(ps);
    ps -> a = (STDatatype*)malloc(sizeof(STDatatype)*4);
    if(ps -> a == NULL)
    {
        perror("malloc fail");
        exit(-1);
    }
    
    ps -> capacity = 4;
    ps -> top = 0;

}

void StackDestroy(ST* ps)
{
    assert(ps);
    free(ps ->a);
    ps ->a = NULL;
    ps ->capacity = ps ->top = 0;

}


void StackPush(ST* ps, STDatatype x)
{
    if (ps -> top == ps -> capacity)
    {
        STDatatype* temp = (STDatatype*)realloc(ps,ps->capacity*sizeof(STDatatype)*2);
        if (temp == NULL)
        {
            perror("realloc fail");
            exit(-1);
        }
        ps -> a = temp;
        ps -> capacity *= 2;
    
    }
    ps -> a[ps -> top] = x;
    ps -> top++;
    
}

void StackPop(ST* ps)
{
    assert(ps);
    ps -> top--;

}

bool StackEmpty(ST* ps)
{
    assert(ps);

    return ps ->top == 0;
}

STDatatype StackTop(ST* ps)
{
    assert(ps);
    assert(!StackEmpty(ps));

    return ps ->a[ps ->top - 1];

}

int StackSize(ST* ps)
{
    assert(ps);
    
    return ps ->top;
}


