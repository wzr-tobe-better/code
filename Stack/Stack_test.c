#include "Stack.h"
void STtest()
{
    ST st;
	StackInit(&st);
	StackPush(&st, 1);
	StackPush(&st, 2);
	StackPush(&st, 3);
	StackPush(&st, 4);
	//StackPush(&st, 5);

	printf("size:%d\n", StackSize(&st)); // 不关心底层实现
	//printf("size:%d\n", st.top);  // 关心
	printf("size:%d\n", st.top+1);  // 关心


	StackPop(&st);
	StackPop(&st);
	StackPop(&st);
	StackPop(&st);
	StackPop(&st);
	//StackPop(&st);
	//printf("%d\n", StackTop(&st));

	StackDestroy(&st);
}
int main()
{
    STtest();
}