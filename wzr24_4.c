#include<stdio.h>
#define M 5
int fun(int a[M][M])
{
	int sum=0,t,i,j;
	for(i=0;i<M;i++)
	{
		for(j=0;j<M;j++)
		{
			if(i==j||i+j==M-1)
			{
				sum=sum+a[i][j];
			}
		}
	}
	return sum;
}
int main()
{
	int i,j,a[M][M],t;
	printf("请输入%d个数:",M*M);
	for(i=0;i<M;i++)
	{
		for(j=0;j<M;j++)
		{
			scanf("%d",&a[i][j]);
		}
	}
	printf("所输入的矩阵为a\n");
	for(i=0;i<M;i++)
	{
		for(j=0;j<M;j++)
		{
			printf("%d\t",a[i][j]);
		}
		printf("\n");
	}

	t=fun(a);
	printf("主副对角线数字之和为%d",t);
}
