#include <stdio.h>

// 定义栈结构
#define MAX_SIZE 100

typedef struct {
    int arr[MAX_SIZE];
    int top;
} Stack;

// 初始化栈
void init(Stack *stack) {
    stack->top = -1;
}

// 判断栈是否为空
int isEmpty(Stack *stack) {
    return stack->top == -1;
}

// 判断栈是否已满
int isFull(Stack *stack) {
    return stack->top == MAX_SIZE - 1;
}

// 入栈
void push(Stack *stack, int value) {
    if (isFull(stack)) {
        printf("Stack is full!\n");
        return;
    }
    stack->arr[++stack->top] = value;
}

// 出栈
int pop(Stack *stack) {
    if (isEmpty(stack)) {
        printf("Stack is empty!\n");
        return -1;
    }
    return stack->arr[stack->top--];
}

// 汉诺塔递归函数
void hanoi(int n, Stack *source, Stack *destination, Stack *auxiliary) {
    if (n == 1) {
        // 只有一个盘子，直接移动到目标塔座
        int disk = pop(source);
        push(destination, disk);
        printf("Move disk %d from tower %c to tower %c\n", disk, source->top + 'A', destination->top + 'A');
    } else {
        // 先将上方 n-1 个盘子从源塔座移动到辅助塔座
        hanoi(n - 1, source, auxiliary, destination);

        // 将最底下的盘子从源塔座移动到目标塔座
        int disk = pop(source);
        push(destination, disk);
        printf("Move disk %d from tower %c to tower %c\n", disk, source->top + 'A', destination->top + 'A');

        // 将辅助塔座上的 n-1 个盘子移动到目标塔座
        hanoi(n - 1, auxiliary, destination, source);
    }
}

int main() {
    int n;
    printf("Enter the number of disks: ");
    scanf("%d", &n);

    Stack source, destination, auxiliary;
    init(&source);
    init(&destination);
    init(&auxiliary);

    // 初始化源塔座
    for (int i = n; i >= 1; i--) {
        push(&source, i);
    }

    // 调用汉诺塔函数
    hanoi(n, &source, &destination, &auxiliary);

    return 0;
}


