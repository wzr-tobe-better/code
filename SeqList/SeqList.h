#pragma once
#define N 10
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
// 静态顺序表
typedef int SLDataType;
typedef struct SeqList
{
    SLDataType* a;
    int size;  //记录存储多少个有效数据
    int capacity;
}SL;

void SLInit(SL* ps);
void SLDestory(SL* ps);
void SLPushBack(SL* ps,SLDataType x); //尾插
void SLPopBack(SL* ps); //尾删



