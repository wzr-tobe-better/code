#include <stdio.h>

void fun(int *p1, int *p2)
{
	int temp;
	if(*p1>*p2){
		temp = *p1;
		*p1 = *p2;
		*p2 = temp;
	}
}
int main()
{
	int a, b, c, *p1=&a, *p2=&b, *p_c=&c;
	printf("请随意输入三个整数：");
	scanf("%d%d%d", p1, p2, p_c);
	fun(p1, p2);
	fun(p1, p_c);
	fun(p2, p_c);
	printf("按照由小到大的顺序输出如下：\n%3d%3d%3d", *p1, *p2, *p_c); 
	return 0;
} 




