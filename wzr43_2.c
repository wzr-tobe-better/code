#include <stdio.h>
#include <stdlib.h>
#define   N   8
struct  slist
{  double   s;
   struct slist  *next;
};
typedef  struct slist  STREC;
double  fun( STREC *h  ){
	STREC *p;
	double max;
	p=h;
    max = p->next->s;
	while(p->next!=0){
		p=p->next;
		if(p->s > max){
			max = p->s;
		}
	}
	return max;
}
STREC * creat( double *s)
{ 
	STREC  *h,*p,*q;
	int  i=0;
	h=p=(STREC*)malloc(sizeof(STREC));
	p->s=0;
	while(i<N)
	{ 
		q=(STREC*)malloc(sizeof(STREC));
		q->s=s[i];
		i++; 
		p->next=q;
		p=q;
	}
	p->next=0;
	return  h;
}
 int outlist( STREC *h)
{ 
	STREC  *p;
	p=h->next;
	printf("head");
	do
	{ 
		printf("->%2.0f",p->s);
		p=p->next;
	}
	while(p!=0);
	printf("\n\n");
}
 
int main()
{  
double  s[N]={85,78,65,85,100,78,65,89}, max;
STREC  *h;
h=creat( s );
outlist(h);
max=fun( h );
printf("max=%6.1f\n",max);
}
