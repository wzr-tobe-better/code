// 写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
// 例如：给定s1 =AABCD和s2 = BCDAA，返回1
// 给定s1=abcd和s2=ACBD，返回0.
// AABCD左旋一个字符得到ABCDA
// AABCD左旋两个字符得到BCDAA
// AABCD右旋一个字符得到DAABC
#include <stdio.h>
#include <string.h>
// int is_left_move(char arr1[],char arr2[])
// {
//     int len1 = strlen(arr1);
//     int len2 = strlen(arr2);
//     // if(len1 != len2)
//     // {
//     //     return 0;
//     // }
//   strncat(arr1,arr1,len1);
//   char* ret = strstr(arr1,arr2);
//   if(ret == NULL)
//     return 0;
//   else
//     return 1;
// }
// int main()
// {
//     char arr1[20] = "abcdef";
//     char arr2[6] = "cdefab";
//     int ret = is_left_move(arr1,arr2);
//     if(1 == ret)
//     {
//         printf("YES");
//     }
//     else
//     {
//         printf("NO");
//     }


//     return 0;
// }
int is_left_move(char arr1[], char arr2[])
{
	int len1 = strlen(arr1);
	int len2 = strlen(arr2);
	if (len1 != len2)
		return 0;

	strncat(arr1,  arr1, len1);
	char* ret = strstr(arr1, arr2);
	if (ret == NULL)
		return 0;
	else
		return 1;
}

//ABCDEF
//ABCDEFABCDEF
int main()
{
	char arr1[] = "ABCDEF";
	char arr2[] = "CDEFAB";
	int ret = is_left_move(arr1, arr2);//判断arr2是否是arr1旋转得到的
	if (1 == ret)
		printf("Yes\n");
	else
		printf("No\n");

	return 0;
}