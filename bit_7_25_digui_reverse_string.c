#include <stdio.h>
#include <string.h>
void reverse_string(char * str)
{
   int len = strlen(str);
   char temp = *str;
   *str = *(str + len - 1);
   *(str + len - 1) = '\0';
   if(strlen(str+1)>1)
   reverse_string(str+1);
   *(str + len - 1) = temp;

}

int main()
{
    char arr[] = "abcdef";
    reverse_string(arr);
    printf("%s\n",arr);
    return 0;
}