#include "ListNode.h"
void LTtest()
{
    LTNode* phead = ListInit();
	LTPushBack(phead, 1);
	LTPushBack(phead, 2);
	LTPushBack(phead, 3);
	LTPushBack(phead, 4);
	LTPushBack(phead, 5);
    LTPopBack(phead);
    LTFind(phead, 3);
    LTPrint(phead);

}
int main()
{
    LTtest();
    return 0;
}