int missingNumber(int* nums, int numsSize){
    int i = 0;
    int N = numsSize;
    int ret = N*(1 + N) / 2 ;
    for(i = 0; i < numsSize; i++)
    {
        ret -= nums[i];
    }
    return ret;

}