# define N 10
#include <stdio.h>
void move_even_odd(int* arr,int sz)
{
    int left = 0;
    int right = sz - 1;
    while (left < right)
    {
        //从左到右找到第一个偶数
    while (arr[left] % 2 == 1)
    {
        left++;
    }
    //从右到左找到第一个奇数
    while (arr[right] % 2 == 0)
    {
        right--;
    }
    if (left < right)
    {
        int temp = arr[left];
        arr[left] = arr[right];
        arr[right] = temp;
        left++;
        right--;

    }
    }
    for (int  i = 0; i < sz; i++)
    {
        printf("%d",arr[i]);
        printf("\n");
    }
        
}
int main()
{
    int arr[N] = {1,2,3,4,5,6,7,8,9,10};
    int sz = sizeof(arr) / sizeof(arr[0]);
    move_even_odd(arr,sz);

    return 0;
}