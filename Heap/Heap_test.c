#include "Heap.h"
#include <time.h>
void TestHeap1()
{
	int array[] = { 27, 15, 19, 18, 28, 34, 65, 49, 25, 37 };
	HP hp;
	HeapInit(&hp);
	for (int i = 0; i < sizeof(array) / sizeof(int); ++i)
	{
		HeapPush(&hp, array[i]);

	}
	HeapPrint(&hp);

	//topK 
	int k = 5;
	while (k--)
	{
		printf("%d ", HeapTop(&hp));
		HeapPop(&hp);
	}

	HeapDestroy(&hp);
}

void TestHeap2()
{
	int array[] = { 27, 15, 19, 18, 28, 34, 65, 49, 25, 37 };
	HP hp;
	HeapInit(&hp);
	for (int i = 0; i < sizeof(array) / sizeof(int); ++i)
	{
		HeapPush(&hp, array[i]);
	}

	while (!HeapEmpty(&hp))
	{
		printf("%d ", HeapTop(&hp));
		HeapPop(&hp);
	}

	HeapDestroy(&hp);
}

void TestHeap3()
{
	int array[] = { 27, 15, 19, 18, 28, 34, 65, 49, 25, 37 };
	HP hp;
	HeapCreate(&hp, array, sizeof(array)/sizeof(int));
	HeapPrint(&hp);

	HeapDestroy(&hp);
}

void HeapSort(int* a, int n)
{
	 //向上调整建堆 -- N*logN
	 for (int i = 1; i < n; ++i)
	 {
	 AdjustUp(a, i);
	 }

	//向下调整建堆 -- O(N)
	//升序：建大堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}

	 //O（N*logN）
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		--end;
	}
}

void TestHeap4()
{
	int array[] = { 27, 15, 19, 18, 28, 34, 65, 49, 25, 37 };
	HeapSort(array, sizeof(array) / sizeof(int));

	for (int i = 0; i < sizeof(array) / sizeof(int); ++i)
	{
		printf("%d ", array[i]);
	}
	printf("\n");
}

// void TestHeap5()
// {
// 	 //造数据
// 	int n = 0; 
//     int k = 0;
// 	printf("请输入n和k:>");
// 	scanf("%d%d", &n, &k);
// 	srand(time(0));
// 	FILE* fin = fopen("data.txt", "w");
// 	if (fin == NULL)
// 	{
// 		perror("fopen fail");
// 		return;
// 	}

// 	int randK = k;
// 	for (int i = 0; i < n; ++i)
// 	{
// 		int val = rand() % 100000;
// 		fprintf(fin, "%d\n", val);
// 	}

// 	fclose(fin);
	
	
// 	//找topk
// 	FILE* fout = fopen("data.txt", "r");
// 	if (fout == NULL)
// 	{
// 		perror("fopen fail");
// 		return;
// 	}

// 	int minHeap[5];
// 	int* minHeap = malloc(sizeof(int)*k);
// 	if (minHeap == NULL)
// 	{
// 		perror("malloc fail");
// 		return;
// 	}

// 	for (int i = 0; i < k; ++i)
// 	{
// 		fscanf(fout, "%d", &minHeap[i]);
// 	}

// 	//建小堆
// 	for (int i = (k - 1 - 1) / 2; i >= 0; --i)
// 	{
// 		AdjustDown(minHeap, k, i);
// 	}

// 	int val = 0;
// 	while (fscanf(fout, "%d", &val) != EOF)
// 	{
// 		if (val > minHeap[0])
// 		{
// 			minHeap[0] = val;
// 			AdjustDown(minHeap, k, 0);
// 		}
// 	}

// 	for (int i = 0; i < k; ++i)
// 	{
// 		printf("%d ", minHeap[i]);
// 	}
// 	printf("\n");

// 	fclose(fout);
// }

void TestHeap6()
{
    int k = 5;
	int minHeap[5];
	FILE* fout = fopen("data.txt", "r");
	if (fout == NULL)
	{
		perror("fopen fail");
		return;
	}

	for (int i = 0; i < k; ++i)
	{
		fscanf(fout, "%d", &minHeap[i]);
	}

	//建小堆
	for (int i = (k - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(minHeap, k, i);
	}

	int val = 0;
	while (fscanf(fout, "%d", &val) != EOF)
	{
		if (val > minHeap[0])
		{
			minHeap[0] = val;
			AdjustDown(minHeap, k, 0);
		}
	}

	for (int i = 0; i < k; ++i)
	{
		printf("%d ", minHeap[i]);
	}
	printf("\n");

	fclose(fout);
}

int main()
{
	TestHeap6();

	return 0;
}