// 实现一个函数，可以左旋字符串中的k个字符。
// 例如：
// ABCD左旋一个字符得到BCDA
// ABCD左旋两个字符得到CDAB
#include <stdio.h>
#include <string.h>
#include <assert.h>
void reverse(char* left, char* right)
{
    while (left < right)
    {
        char temp = *left;
        *left = *right;
        *right = temp;
        left++;
        right--;
    }
    
}
void left_move(char arr[],int k,int len)
{
    k %= len;
    reverse(arr,arr + k - 1);
    reverse(arr + k,arr + len - 1);
    reverse(arr,arr + len - 1);

}
int main()
{
    char arr[] = "abcdefghi";
    int len = strlen(arr);
    int k = 0;
    int i = 0;
    scanf("%d",&k);
    left_move(arr,k,len);
    for(i = 0; i < len; ++i)
    {
        printf("%c",arr[i]);
    }
    return 0;

}