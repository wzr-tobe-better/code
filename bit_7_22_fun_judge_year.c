#include <stdio.h>
void fun(int x)
{
    if((x%4==0 && x%100!=0) || (x%400==0))
    {
        printf("%d是闰年",x);
    }
    else
    {
        printf("%d不是闰年\n",x);
    }
}

int main()
{
    int year = 0;
    scanf("%d",&year);
    fun(year);
    return 0;
}