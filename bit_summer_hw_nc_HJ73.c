#include <stdio.h>
int main()
{
    int year = 0;
    int month = 0;
    int day = 0;
    int sum = 0;
    int month_day[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    scanf("%d %d %d",&year,&month,&day);
    if((year % 4 == 0 && year % 100 !=0) || (year % 400 == 0) &&(month > 2))
    {
        sum += 1 + day;
        int i = 0;
        for(i = month - 1; i > 0; i--)
        {
            sum += month_day[i];
        }
        
    }
    else
    {
        sum += day;
        int i = 0;
        for(i = month - 1; i > 0; i--)
        {
            sum += month_day[i];
        }
    }
    printf("%d",sum);
    return 0;
}