#include "SeqList.h"
void SLPrint(SL* ps)
{
    assert(ps);
    for (int i = 0; i < ps -> size; ++i)
    {
        printf("%d",ps -> a[i]);
    }
    printf("\n");
    
}
void SLInit(SL* ps)
{
    assert(ps);
    ps->a = NULL;
    ps->size = 0;
    ps->capacity = 0;

}

void SLDestroy(SL* ps)
{
    assert(ps);
    if (ps ->a)
    {
        free(ps ->a);
        ps ->a = NULL;
        ps ->size = ps ->capacity = 0;

    }
    

}
void SLPushBack(SL* ps,SLDataType x)
{
    assert(ps);
    if (ps -> size == ps -> capacity)
    {
        int newcapacity = ps -> capacity == 0 ? 4 : ps -> capacity * 2;
        SLDataType* tmp = (SLDataType*)realloc(ps ->a,newcapacity*sizeof(SLDataType));
        if (tmp == NULL)
        {
            perror("realloc fail");
            exit(-1);
        }
        ps ->a = tmp;
        ps ->capacity = newcapacity;
    }

    ps -> a[ps -> size] = x;
    ps ->size++;
    
}
void SLPopBack(SL* ps)
{
    assert(ps ->size > 0);
    ps ->size--;
}
