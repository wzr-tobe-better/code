#define Max 1001
#define MaxSize 100
#include<stdio.h>
#include<stdlib.h>
typedef struct MGraph
{
	int vertex[MaxSize]; 
	int edge[MaxSize][MaxSize]; 
	int vertexNum,edgeNum; 
}MG;
//2)构造一个图 
MG  CreatGraph(int n,int m)
{
	MG G;
	int i = 0;
    int j = 0;
    int a = 0;
    int b = 0;
    int c = 0;
	G.vertexNum=n;
	G.edgeNum=m;
	for(i=1;i<=G.vertexNum;i++)
	{
		G.vertex[i]=i;
	} 

	for(i=1;i<=G.vertexNum;i++)
	{
		for(j=1;j<=G.vertexNum;j++)
		{
			if(i==j)
			{
				G.edge[i][j]=0;
			} 
			else
			{
				G.edge[i][j]=Max;
			}	
		}
	}
	 
	for(i=1;i<=G.edgeNum;i++)
	{
		scanf("%d %d %d",&a,&b,&c);
		G.edge[a][b]=c;
		G.edge[b][a]=c; 
	} 
	return G;
}

void Dijkstra(MG G, int v,int n)
{
    int i = 0;
    int k = 0;
    int num = 0;
    int dist[n];
    int d[n];  
    for (i = 2; i <=G.vertexNum; i++)
    {
        dist[i] = G.edge[v][i];   
    }
    for (num = 1; num < G.vertexNum; num++)
    {
		for (k = 2, i = 2; i <=G.vertexNum; i++)  
        { 
			if(dist[k]==0)
			{
				for(k=2;k<=G.vertexNum;k++)
				{
					if((dist[k]==0)&&(dist[k+1]!=0))
					{
						k++;
						break;
					}
				}
			}
			if ((dist[i] != 0) && (dist[i] < dist[k])) 
            {
            	k = i;
            }
        }
        for (i = 2; i <=G.vertexNum; i++)
        {
            if (dist[i] > dist[k] + G.edge[k][i]) 
            {
                dist[i] = dist[k] + G.edge[k][i];  
            }
        }
        d[k]=dist[k];
        dist[k] = 0;
    }
    printf("%d",d[G.vertexNum]);
} 
int main()
{
	int n = 0;
    int m = 0;
	scanf("%d %d",&n,&m);
	MG G;
	G=CreatGraph(n,m);
	int v = 1;
	Dijkstra(G,v,n); 

	return 0;
}