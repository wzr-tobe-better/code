// #include<iostream>
// using namespace std;

// class Person
// {
// public:
// 	void fun()
// 	{
// 		cout << "Person::func()" << endl;
// 	}

// protected:
// 	string _name = "小李子"; // 姓名
// 	int _num = 111; 	   // 身份证号
// };

// // 隐藏/重定义：子类和父类有同名成员，子类的成员隐藏了父类的成员
// class Student : public Person
// {
// public:
// 	void fun()
// 	{
// 		cout << "Student::func()" << endl;
// 	}

// 	void Print()
// 	{
// 		cout << " 姓名:" << _name << endl;
// 		cout << _num << endl;//访问类域
// 		cout << Person::_num << endl;//由于隐藏了父类的成员，需要指定类域访问
// 	}
// protected:
// 	int _num = 999; // 学号
// };

// int main()
// {
// 	Student s;
// 	s.Print();
// 	s.fun();
// 	s.Person::fun();

// 	return 0;
// }

// #include<iostream>
// using namespace std;

// class Person
// {
// public:
// 	Person(const char* name = "peter")
// 		: _name(name)
// 	{
// 		cout << "Person()" << endl;
// 	}

// 	Person(const Person& p)
// 		: _name(p._name)
// 	{
// 		cout << "Person(const Person& p)" << endl;
// 	}

// 	Person& operator=(const Person& p)
// 	{
// 		cout << "Person operator=(const Person& p)" << endl;
// 		if (this != &p)
// 			_name = p._name;

// 		return *this;
// 	}

// 	~Person()
// 	{
// 		cout << "~Person()" << endl;
		
// 	}
// protected:
// 	string _name; // 姓名
// };

// class Student : public Person
// {
// public:
// 	// 先父后子
// 	Student(const char* name, int id = 0)
// 		:Person(name)
// 		,_id(0)
// 	{}

// 	Student(const Student& s)
// 		:Person(s)//显示调用父类的拷贝构造，传s会切片
// 		,_id(s._id)
// 	{}

// 	Student& operator=(const Student& s)
// 	{
// 		if (this != &s)
// 		{
// 			Person::operator=(s);//由于构成隐藏需要指定作用域
// 			_id = s._id;
// 		}

// 		return *this;
// 	}

// 	~Student()
// 	{
// 		// 由于后面多态的原因(具体后面讲)，析构函数的函数名被
// 		// 特殊处理了，统一处理成destructor

// 		// 显示调用父类析构，无法保证先子后父
// 		// 所以子类析构函数完成就，自定调用父类析构，这样就保证了先子后父
// 		cout<<"~Student()" <<endl;
// 	}
// protected:
// 	int _id;

// 	int* _ptr = new int;
// };

// int main()
// {
// 	Student s1("jack", 18);
// 	Student s2(s1);

// 	Student s3("李四", 1);
// 	s1 = s3;

// 	return 0;
// }

#include <iostream>
#include <vector>
using namespace std;

int main() 
{
    int n = 0;
    cin >> n;
 
        vector<int> v;
        while (n)
        {
            int a = n % 10;
            v.push_back(a);
            n /= 10;
        }
    for (size_t i = 0; i < v.size(); i++)
    {
        cout << v[i] << endl;
    }
    
}