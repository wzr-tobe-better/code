#include <iostream>
#include <vector>
using namespace std;

template<class T>
class Stack
{
public:
	Stack(int capaicty = 4)
	{
		_a = new T[capaicty];
		_top = 0;
		_capacity = capaicty;
	}

	~Stack()
	{
		delete[] _a;
		_capacity = _top = 0;
	}

private:
	T* _a;
	size_t _top;
	size_t _capacity;
};

int main()
{
    Stack<int> st1;
    Stack<double> st2;

    vector<int> v ;

    v.push_back(1);
	for (size_t i = 0; i < v.size(); ++i)
	{
       
		cout << v[i] << " ";
	}

	return 0;
}