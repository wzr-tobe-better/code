#include <stdio.h>
int fib(int n) //递归
{
    if(n<3)
    {
        return 1;
    }
    else
    {
        return fib(n-1)+fib(n-2);
    }
}

int main()
{
    int n = 0;
    scanf("%d",&n);
    printf("%d\n",fib(n));
    return 0;
}