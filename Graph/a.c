#include<stdio.h>
#include<stdlib.h>
#define MaxInt 32767
#define MVNum 100
typedef char VerTexType;    //顶点类型
typedef int ArcType;        //边上的权值类型
typedef struct {
    VerTexType vexs[MVNum];    //顶点表
    ArcType arcs[MVNum][MVNum];    //邻接矩阵
    int vexnum, arcnum;    //图的当前顶点数和边数
} AMGraph;
//邻接矩阵的创建
void CreateUDN(AMGraph *G) {
    int i, j, k;
    printf("请输入顶点数和边数：");
    scanf("%d%d", &G->vexnum, &G->arcnum);
    printf("请输入顶点信息：");
    for (i = 0; i < G->vexnum; i++) {
        scanf("%c", &G->vexs[i]);
    }
    for (i = 0; i < G->vexnum; i++) {
        for (j = 0; j < G->vexnum; j++) {
            G->arcs[i][j] = MaxInt;
        }
    }
    printf("请输入边的信息：");
    for (k = 0; k < G->arcnum; k++) {
        scanf("%d%d", &i, &j);
        scanf("%d", &G->arcs[i][j]);
        G->arcs[j][i] = G->arcs[i][j];
    }
}
//邻接矩阵的输出
void DispAdj(AMGraph *G) {
    int i, j;
    for (i = 0; i < G->vexnum; i++) {
        for (j = 0; j < G->vexnum; j++) {
            printf("%d ", G->arcs[i][j]);
        }
        printf("\n");
    }
}
int main() {
    AMGraph G;
    CreateUDN(&G);
    DispAdj(&G);
    return 0;
}