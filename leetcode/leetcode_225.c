#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef int QDataType;
typedef struct QueueNode
{
    QDataType data;
    struct QueueNode* next;

}QNode;

typedef struct Queue
{
    QNode* head;
    QNode* tail;
    int size;
}Queue;

void QueueInit(Queue* pq);
void QueueDestroy(Queue* pq);
void QueuePush(Queue* pq, QDataType x);
void QueuePop(Queue* pq);
QDataType QueueFront(Queue* pq);
QDataType QueueBack(Queue* pq);
bool QueueEmpty(Queue* pq);
int QueueSize(Queue* pq);

void QueueInit(Queue* pq)
{
    assert(pq);
    pq ->head = NULL;
    pq ->tail = NULL;
    pq ->size = 0;
}
void QueueDestroy(Queue* pq)
{
    assert(pq);
    QNode* cur = pq ->head;
    while (cur)
    {
        QNode* del = cur;
        cur = cur ->next;
        free(del);
    }

    pq ->head = pq ->tail = NULL;
    pq ->size = 0;
    
}
void QueuePush(Queue* pq, QDataType x)
{
    assert(pq);
    QNode* newnode = (QNode*)malloc(sizeof(QNode));
    if (newnode == NULL)
    {
        perror("malloc fail");
        exit(-1);
    }

    newnode ->data = x;
    newnode ->next = NULL;

    if (pq ->tail == NULL)
    {
        pq ->head = pq ->tail = newnode;
    }
    else
    {
        pq ->tail ->next = newnode;
        pq ->tail = newnode;
    }
    pq ->size++;
}
void QueuePop(Queue* pq)
{
    assert(pq);
    assert(!QueueEmpty(pq));
    if (pq ->head ->next == NULL)
    {
        free(pq ->head);
        pq ->head = pq->tail = NULL;

    }
    else
    {
        QNode* del = pq ->head;
        pq ->head = pq ->head ->next;
        free(del);
    }
    pq ->size--;
}
QDataType QueueFront(Queue* pq)
{
    assert(pq);
	assert(!QueueEmpty(pq));
    return pq ->head ->data;
}
QDataType QueueBack(Queue* pq)
{
    assert(pq);
	assert(!QueueEmpty(pq));
    return pq ->tail ->data;
}
bool QueueEmpty(Queue* pq)
{
    assert(pq);
    return pq ->head == NULL && pq ->tail == NULL;
}
int QueueSize(Queue* pq)
{
    assert(pq);
    return pq ->size;
}

typedef struct {
    Queue q1;
    Queue q2;
} MyStack;


MyStack* myStackCreate() {
    MyStack* obj = (MyStack*)malloc(sizeof(MyStack));
    QueueInit(&obj ->q1);
    QueueInit(&obj ->q2);
    
    return obj;
}

void myStackPush(MyStack* obj, int x) {
    if(!QueueEmpty(&obj ->q1))
    {
        QueuePush(&obj ->q1,x);
    }
    else
    {
        QueuePush(&obj ->q2,x);

    }

}

int myStackPop(MyStack* obj) {
    Queue* emptyQ = &obj ->q1;
    Queue* noemptyQ = &obj ->q2;
    if(!QueueEmpty(&obj ->q1))
    {
        emptyQ = &obj ->q2;
        noemptyQ = &obj ->q1;
    }

    //非空队列前n-1个数据倒入空队列
    while(QueueSize(noemptyQ) > 1)
    {
        QueuePush(emptyQ,QueueFront(noemptyQ));
        QueuePop(noemptyQ);
    }

    int top = QueueFront(noemptyQ);
    QueuePop(noemptyQ);

    return top;

}

int myStackTop(MyStack* obj) {
    if(!QueueEmpty(&obj ->q1))
    {
        return QueueBack(&obj ->q1);
    }
    else
    {
        return QueueBack(&obj ->q2);
    }

}

bool myStackEmpty(MyStack* obj) {
    return QueueEmpty(&obj ->q1) && QueueEmpty(&obj ->q2);

}

void myStackFree(MyStack* obj) {
    QueueDestroy(&obj ->q1);
    QueueDestroy(&obj ->q2);
    free(obj);

}