#include <stdio.h>
 struct ListNode {
      int val;
     struct ListNode *next;
 };
struct ListNode* removeElements(struct ListNode* head, int val)
{
    struct ListNode* cur = head;
    struct ListNode* newhead = NULL;
    struct ListNode* tail = NULL;
    while(cur)
    {
        if(cur -> val != val)
        {
            if(tail == NULL)
            {
                newhead = tail = cur;
            }
            else
            {
                tail -> next = cur;
                tail = tail -> next;
            }
            cur = cur -> next;
        }
        else
        {
            struct ListNode* next = cur -> next;
            free(cur);
            cur = next;
        }
    }
    if(tail != NULL)
    {
       tail -> next = NULL;
    }
    return newhead;
}

