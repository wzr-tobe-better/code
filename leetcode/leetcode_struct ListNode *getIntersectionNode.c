#include "leetcode_ struct ListNode .h"
struct ListNode *getIntersectionNode(struct ListNode *headA, struct ListNode *headB) {
    struct ListNode* curA = headA;
    struct ListNode* curB = headB;
    int lenA = 0;
    int lenB = 0;
    while(curA ->next)
    {
        ++lenA;
        curA = curA ->next;
    }

       while(curB ->next)
    {
        ++lenB;
        curB = curB ->next;
    }

    if(curA != curB)
    {
        return NULL;
    }
    int gap = abs(lenA - lenB);
    struct ListNode* longlist = headA;
    struct ListNode* shortlist = headB;
    if(lenB > lenA)
    {
        longlist = headB;
        shortlist = headA;
    }

    while(gap--)
    {
        longlist = longlist ->next;
    }

    while(longlist != shortlist)
    {
        longlist = longlist ->next;
        shortlist = shortlist ->next;
    }

    return longlist;


    
}