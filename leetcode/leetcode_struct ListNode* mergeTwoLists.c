#include <leetcode_ struct ListNode .h>
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2)
{
    struct ListNode* newhead = NULL;
    struct ListNode* tail = NULL;
    if(list1 == NULL)
    {
        return list2;
    }
      if(list2 == NULL)
    {
        return list1;
    }
    while(list1 && list2)
    {
        if(list1 -> val < list2 -> val)
        {
            if(tail == NULL)
            {
                newhead = tail = list1;
            }
            else
            {
                tail -> next = list1;
                tail = tail -> next;
            }
            list1 = list1 -> next;

        }
        else
        {
             if(tail == NULL)
            {
                newhead = tail = list2;
            }
            else
            {
                tail -> next = list2;
                tail = tail -> next;
            }
            list2 = list2 -> next;

        }
    }
    if(list1)
    {
        tail -> next = list1;
    }
    if(list2)
    {
        tail -> next = list2;
    }

        return newhead;

}