

 struct ListNode {
      int val;
      struct ListNode *next;
  };

#include <stdio.h>
struct ListNode* reverseList(struct ListNode* head)
{
    struct ListNode* cur = head;
    struct ListNode* rhead = NULL;
    while(cur)
    {
       struct ListNode* next = cur -> next;
       cur -> next = rhead;
       rhead = cur;
       cur = next;
    }

    return rhead;
}