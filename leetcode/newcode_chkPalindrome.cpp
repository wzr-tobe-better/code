#include "leetcode_ struct ListNode .h"
class PalindromeList {
public:
    struct ListNode* middleNode(struct ListNode* head)
{
    struct ListNode* slow = head;
    struct ListNode* fast = head;
    while(fast && fast -> next)
    {
        slow = slow -> next;
        fast = fast -> next -> next;
    }
    
    return slow;

}
    
   struct ListNode* reverseList(struct ListNode* head)
{
    struct ListNode* cur = head;
    struct ListNode* rhead = NULL;
    while(cur)
    {
       struct ListNode* next = cur -> next;
       cur -> next = rhead;
       rhead = cur;
       cur = next;
    }

    return rhead;
} 
    bool chkPalindrome(ListNode* A) {
        struct ListNode* mid = middleNode(A);
        struct ListNode* rhead = reverseList(mid);
        while (A && rhead) 
        {
            if (A -> val != rhead -> val) 
            {
                return false;
            
            }
            A = A -> next;
            rhead = rhead -> next;
        
        }
        return true;
    }
};