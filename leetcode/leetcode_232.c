#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
typedef int STDataType;
typedef struct Stack
{
	STDataType* _a;
	int _top; // 栈顶
	int _capacity; // 容量
}Stack;
// 初始化栈
void StackInit(Stack* ps)
{
	ps->_a = NULL;
	ps->_capacity = ps->_top = 0;
}

// 检测栈是否为空，如果为空返回true，非空返回false
bool StackEmpty(Stack* ps)
{
	return ps->_top == 0;
}

// 入栈
void StackPush(Stack* ps, STDataType data)
{
	if (ps->_top == ps->_capacity)				//扩容
	{
		STDataType newcapacity = ps->_capacity == 0 ? 4 : ps->_capacity * 2;
		STDataType * tmp = (STDataType * )realloc(ps->_a, newcapacity * sizeof(Stack));
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		ps->_capacity = newcapacity;
		ps->_a = tmp;
	}
	 ps->_a[ps->_top] = data;
	 ps->_top++;
}
// 出栈
void StackPop(Stack* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));    //判空，为假报错
	ps->_top--;
}
// 获取栈顶元素
STDataType StackTop(Stack* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));    //判空，为假报错
	return ps->_a[ps->_top-1];
}
// 获取栈中有效元素个数
int StackSize(Stack* ps)
{
	assert(ps);
	STDataType size = ps->_top;
	return size;
}

// 销毁栈
void StackDestroy(Stack* ps)
{
	free(ps->_a);
	ps->_a = NULL;
	ps->_capacity = ps->_top = 0;
}

//My Queue
typedef struct 
{
    Stack Pushst;
    Stack Popst;
} MyQueue;

//MYQUEUE初始化
MyQueue* myQueueCreate() 
{
    MyQueue*obj = (MyQueue*)malloc(sizeof(MyQueue));
    if(obj==NULL)
    {
        perror("malloc fail");
        return NULL;
    }
    StackInit(&obj->Pushst);
    StackInit(&obj->Popst);
    return obj;
}


//将元素推到队列的末尾
void myQueuePush(MyQueue* obj, int x)
{
    StackPush(&obj->Pushst , x);   //把数据全部传入到Pushst队列
}

//获取队列开头的元素
int myQueuePeek(MyQueue* obj) 
{
    //当Popst为空，捯数据
    if(StackEmpty(&obj->Popst))
    {
        
        while(!StackEmpty(&obj->Pushst))
        {
            //捯数据到Popst中
            StackPush(&obj->Popst,StackTop(&obj->Pushst));
            StackPop(&obj->Pushst);
        }
        //返回栈顶元素即先进先出的队列的第一个元素
        return StackTop(&obj->Popst);
    }
    else   
    {
        return StackTop(&obj->Popst);
    }
}

//移除队头并返回
int myQueuePop(MyQueue* obj) 
{
    int Qhead = myQueuePeek(obj);
    StackPop(&obj->Popst);
    return Qhead;
}

//判空
bool myQueueEmpty(MyQueue* obj) 
{
    //两个队列都要为空才为空
    return StackEmpty(&obj->Popst) && StackEmpty(&obj->Pushst);
}

void myQueueFree(MyQueue* obj) 
{
    StackDestroy(&obj->Pushst);
    StackDestroy(&obj->Popst);
    free(obj);
}
