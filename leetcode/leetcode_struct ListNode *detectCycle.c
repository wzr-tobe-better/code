#include "leetcode_ struct ListNode .h"
struct ListNode *detectCycle(struct ListNode *head) {
    struct ListNode* slow = head;
    struct ListNode* fast = head;
    while(fast && fast ->next)
    {
        slow = slow ->next;
        fast = fast ->next ->next;
        
        if(fast == slow)
        {
            struct ListNode* meet = slow;
            struct ListNode* cur = head;
            while(meet != cur)
            {
                cur = cur ->next;
                meet = meet ->next;
            }

            return meet;
        }
    }

    return NULL;
}