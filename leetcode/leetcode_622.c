#include "leetcode_ struct ListNode .h"
typedef struct {
    int* a;
    int front;
    int rear;
    int k;

} MyCircularQueue;


MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* obj = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    //多开一个，解决判满问题
    obj ->a = (int*)malloc(sizeof(int)*(k+1));
    obj ->front = obj ->rear = 0;
    // 满的时候能存储数据个数，空间大小应该是k+1;
    obj ->k = k;

    return obj;

}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    assert(obj);
    return obj ->rear == obj ->front;

}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    assert(obj);
    return ((obj ->rear+1) % (obj ->k+1)) == obj ->front;

}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    assert(obj);
    if(myCircularQueueIsFull(obj))
    {
        return false;
    }

    obj ->a[obj ->rear++] = value;
    obj ->rear %= (obj ->k+1);
    return true;
   
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
     assert(obj);

    if(myCircularQueueIsEmpty(obj))
    {
        return false;
    }

    obj ->front++;
    obj ->front %= (obj ->k+1);
    return true;

}

int myCircularQueueFront(MyCircularQueue* obj) {
    assert(obj);
    if(myCircularQueueIsEmpty(obj))
    {
        return -1;
    }
    else
    {
        return obj ->a[obj ->front];
    }

}

int myCircularQueueRear(MyCircularQueue* obj) {
    assert(obj);
    if(myCircularQueueIsEmpty(obj))
    {
        return -1;
    }
    else
    {
        return obj ->a[(obj ->rear + obj ->k) % (obj ->k + 1)] ;
    }

}



void myCircularQueueFree(MyCircularQueue* obj) {
    free(obj ->a);
    free(obj);

}

