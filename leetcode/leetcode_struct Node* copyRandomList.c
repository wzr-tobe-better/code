#include "leetcode_ struct ListNode .h"
struct ListNode* copyRandomList(struct ListNode* head) {
	//拷贝节点链接到原节点后面
    struct ListNode* cur = head;
    while(cur)
    {
        struct ListNode* next = cur ->next;
        struct ListNode* copy = (struct ListNode*)malloc(sizeof(struct ListNode));
        copy ->val = cur ->val;

        cur ->next = copy;
        copy ->next = next;
        cur = next;
    }

    cur = head;
    while(cur)
    {
        struct ListNode* copy = cur ->next;

        if(cur ->random == NULL)
        {
            copy ->random = NULL;

        }
        else
        {
            copy ->random = cur ->random ->next;
        }

        cur = cur ->next ->next;
    }

    //解下来单独链接起来
    struct ListNode* copyHead = NULL, *copyTail = NULL;
    while(cur)
    {
        struct ListNode* copy = cur ->next;
        struct ListNode* next = copy ->next;

        cur ->next = next;
        if(copyTail == NULL)
        {
            copyHead = copyTail = copy;
        }
        else
        {
            copyTail ->next = copy;
            copyTail = copyTail ->next;
        }
        cur = next;
    }

    return copyHead;
}