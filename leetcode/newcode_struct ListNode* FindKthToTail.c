#include "leetcode_ struct ListNode .h"
struct ListNode* FindKthToTail(struct ListNode* pListHead, int k ) 
{
    struct ListNode* slow = pListHead;
    struct ListNode* fast = pListHead;
    while (k--) 
    {
        if (fast == NULL) 
        {
            return NULL;
        
        }
        fast = fast -> next;
    
    }
    while (fast) 
    {
        slow = slow -> next;
        fast = fast -> next;
    
    }
    
    return slow;

}