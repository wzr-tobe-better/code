import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame("计算器");
        frame.setContentPane(new MyForm().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
