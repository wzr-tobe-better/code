import java.util.Stack;

public class Calculator {

    public String cal(String str) {
        str = str.replaceAll("(?<![0-9)}\\]])(?=-[0-9({\\[])", "0") + "#";
        //初始化栈
        Stack<Double> operandStack = new Stack<>();
        Stack<Character> operatorStack = new Stack<>();

        //分析计算
        int strLen = str.length();
        String temp = "";
        for (int i = 0; i < strLen; i++) {
            char x = str.charAt(i);
            //若为数字，记入temp中;否则转double入栈
            if (x >= '0' && x <= '9' || x == '.') {
                temp += x;
            } else {
                if (!temp.isEmpty()) {
                    double a = Double.parseDouble(temp);
                    operandStack.push(a);
                    temp = "";
                }

                if (x == '#') {
                    break;
                } else if (x == '+' || x == '-') {
                    if (operatorStack.empty() || operatorStack.peek() == '(') {
                        operatorStack.push(x);
                    } else {
                        while (!operatorStack.empty() && operatorStack.peek() != '(') {
                            //出栈计算
                            pop2Cal(operandStack, operatorStack);
                        }
                        operatorStack.push(x);

                    }
                } else if (x == '*' || x == '/') {
                    if (operatorStack.empty() || operatorStack.peek() == '(' || operatorStack.peek() == '+' || operatorStack.peek() == '-') {
                        operatorStack.push(x);
                    } else {
                        while (!operatorStack.empty() && operatorStack.peek() != '(' && operatorStack.peek() != '+' && operatorStack.peek() != '-') {
                            //出栈计算
                            pop2Cal(operandStack, operatorStack);
                        }
                        operatorStack.push(x);
                    }
                } else {
                    if (x == '(') {
                        operatorStack.push(x);
                    } else if (x == ')') {
                        while (operatorStack.peek() != '(') {
                            //出栈计算
                            pop2Cal(operandStack, operatorStack);
                        }
                        operatorStack.pop();
                    }
                }
            }
        }
        while (!operatorStack.empty()) {
            //出栈计算
            pop2Cal(operandStack, operatorStack);
        }
        //返回结果
        return operandStack.pop().toString();
    }

    //出栈计算
    private static void pop2Cal(Stack<Double> opStack, Stack<Character> otStack) {
        double op = 0;
        double op2 = opStack.pop();
        double op1 = opStack.pop();
        char ot = otStack.pop();
        switch (ot) {
            case '+':
                op = op1 + op2;
                break;
            case '-':
                op = op1 - op2;
                break;
            case '*':
                op = op1 * op2;
                break;
            case '/':
                op = op1 / op2;
                break;
        }
        opStack.push(op);
    }

}
