//import java.util.Scanner;
//import java.util.Stack;
//
////测试用源代码
//public class Arithmetic {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        while (sc.hasNextLine()) {
//            String str = sc.nextLine().replaceAll("(?<![0-9)}\\]])(?=-[0-9({\\[])", "0") + "#";
//            //字符串处理[],{}
//            str = str.replaceAll("[\\[\\{]", "(");
//            str = str.replaceAll("[\\]\\}]", ")");
//            //System.out.println(str);
//            System.out.println(calculate(str));
//        }
//    }
//
//    private static double calculate(String str) {
//        //初始化栈
//        Stack<Double> operandStack = new Stack<>();
//        Stack<Character> operatorStack = new Stack<>();
//
//        //分析计算
//        int strLen = str.length();
//        String temp = "";
//        for (int i = 0; i < strLen; i++) {
//            char x = str.charAt(i);
//            //若为数字，记入temp中;否则转double入栈
//            if (x >= '0' && x <= '9' || x == '.') {
//                temp += x;
//            } else {
//                if (!temp.isEmpty()) {
//                    double a = Double.parseDouble(temp);
//                    operandStack.push(a);
//                    temp = "";
//                }
//
//                if (x == '#') {
//                    break;
//                } else if (x == '+' || x == '-') {
//                    if (operatorStack.empty() || operatorStack.peek() == '(') {
//                        operatorStack.push(x);
//                    } else {
//                        while (!operatorStack.empty() && operatorStack.peek() != '(') {
//                            //出栈计算
//                            pop2Cal(operandStack, operatorStack);
//                        }
//                        operatorStack.push(x);
//
//                    }
//                } else if (x == '*' || x == '/') {
//                    if (operatorStack.empty() || operatorStack.peek() == '(' || operatorStack.peek() == '+' || operatorStack.peek() == '-') {
//                        operatorStack.push(x);
//                    } else {
//                        while (!operatorStack.empty() && operatorStack.peek() != '(' && operatorStack.peek() != '+' && operatorStack.peek() != '-') {
//                            //出栈计算
//                            pop2Cal(operandStack, operatorStack);
//                        }
//                        operatorStack.push(x);
//                    }
//                } else {
//                    if (x == '(') {
//                        operatorStack.push(x);
//                    } else if (x == ')') {
//                        while (operatorStack.peek() != '(') {
//                            //出栈计算
//                            pop2Cal(operandStack, operatorStack);
//                        }
//                        operatorStack.pop();
//                    }
//                }
//            }
//        }
//        while (!operatorStack.empty()) {
//            //出栈计算
//            pop2Cal(operandStack, operatorStack);
//        }
//        //返回结果
//        return operandStack.pop();
//    }
//
//    /**
//     * 出栈计算
//     * @param opStack  操作数栈
//     * @param otStack  操作符栈
//     */
//    private static void pop2Cal(Stack<Double> opStack, Stack<Character> otStack) {
//        double op = 0;
//        double op2 = opStack.pop();
//        double op1 = opStack.pop();
//        char ot = otStack.pop();
//        switch (ot) {
//            case '+':
//                op = op1 + op2;
//                break;
//            case '-':
//                op = op1 - op2;
//                break;
//            case '*':
//                op = op1 * op2;
//                break;
//            case '/':
//                op = op1 / op2;
//                break;
//        }
//        opStack.push(op);
//    }
//}