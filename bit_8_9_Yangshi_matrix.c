// 有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，
// 请编写程序在这样的矩阵中查找某个数字是否存在。
#include <stdio.h>
void find_num_Yangshi_matrix(int arr[3][3],int k,int *px,int *py)
{
    int i = 0;
    int j = *py - 1;
    int flag = 0;
    while (i <= *px && *py >= 0)
    {
        if (arr[i][j] < k)
    {
        i++;
    }
    else if(arr[i][j] > k)
    {
        j--;
    }
    else
    {
        flag = 1;
        *px = i;
        *py = j;
        break;
    }

    }
    
    if (flag == 0)
    {
         *px = -1;
         *py = -1;
    }
      
}
int main()
{
    int arr[3][3] = {1,2,3,4,5,6,7,8,9};
    int k = 0;
    scanf("%d",&k);
    int x = 3;
    int y = 3;
    find_num_Yangshi_matrix(arr,k,&x,&y);
    if(x == -1 && y == -1)
    {
        printf("没找到\n");
    }
    printf("找到了，下标为 %d %d\n",x,y);
    return 0;
}