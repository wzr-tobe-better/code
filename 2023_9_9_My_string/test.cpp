#include "Mystring.h"

void test_string1()
{
    w ::string s1("hello world");
    cout << s1.c_str() << endl;

    for (size_t i = 0; i < s1.size(); i++)
    {
        cout << s1[i] << " ";
    }
    cout << endl;

    w ::string::iterator it = s1.begin();
    while (it != s1.end())
    {
        cout << *it << " ";
        ++it;
    }
    cout <<endl;
    
    for (auto ch : s1)
    {
        cout << ch <<" ";
    }
    cout <<endl;
}

void test_string2()
{

	w::string s1("hello world");
	cout << s1.c_str() << endl;

	s1.push_back(' ');
	s1.push_back('#');
	s1.append("hello");
	cout << s1.c_str() << endl;

    w::string s2("hello world");
	cout << s2.c_str() << endl;

	s2 += ' ';
	s2 += '#';
	s2 += "hello code";
	cout << s2.c_str() << endl;

}

void test_string3()
{
	w::string s1("helloworld");
	cout << s1.c_str() << endl;

	s1.insert(5, 3, '#');
	cout << s1.c_str() << endl;

	s1.insert(0, 3, '#');
	cout << s1.c_str() << endl;

    w::string s2("helloworld");
	s2.insert(5, "%%%%%");
	cout << s2.c_str() << endl;
	
}

void test_string4()
{
	w::string s1("helloworld");
	cout << s1.c_str() << endl;

	s1.erase(5, 3);
	cout << s1.c_str() << endl;

	s1.erase(5, 30);
	cout << s1.c_str() << endl;

	s1.erase(2);
	cout << s1.c_str() << endl;
}

void test_string5()
{
	w::string s1("helloworld");
	cout << s1.find('w',2) << endl;

	
}

void test_string6()
{
	w::string s1("hello world");
	w::string s2(s1);

	cout << s1.c_str() << endl;
	cout << s2.c_str() << endl;

}

void test_string7()
{
	w::string s("hello world");
	s.resize(8);
	cout << s.c_str() << endl;
	cout << s << endl;

	s.resize(13, 'x');
	cout << s.c_str() << endl;
	cout << s << endl;

	s.resize(20, 'y');
	cout << s.c_str() << endl;
	cout << s << endl;
	
}

void test_string8()
{
	w::string s("hello world");
	cin >> s;
	cout << s << endl;
}

void test_string9()
{
	//string s1("bb");
	//string s2("aaa");
	//cout << (s1 < s2) << endl;

	w::string s1("hello");
	s1 += '\0';
	s1 += "1";
	w::string s2("hello");
	s2 += '\0';
	s2 += "2";
	cout << (s1 < s2) << endl;

}


void test_string10()
{
	w::string s1("hello");
	w::string s2(s1);

	cout << s1 << endl;
	cout << s2 << endl;

	w::string s3("xxxxxxxxxxxxx");
	s1 = s3;

	cout << s1 << endl;
	cout << s3 << endl;
}


int main()
{
    test_string10();
    return 0;
}