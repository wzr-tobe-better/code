#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#define N 3
void fun(int a[][N], int n)
{
    int i;
    int j;
    for(i=0;i<N;i++)
    {
    for(j=0;j<=i;j++)
    {
        if(i>=j)
    a[i][j]=n*a[i][j];
    }
    }
}

int main()
{
    int a[N][N];
    int i;
    int j;
    int n;
    printf("随机产生的数组为\n");
    srand (time(NULL));
    for(i=0;i<N;i++)
    {
        for(j=0;j<N;j++)
        {
            a[i][j]=rand ()%10;
            printf("%4d",a[i][j]);
        }
        printf("\n");
    }
    printf("输入n的值:\n");
    scanf("%d",&n);
    fun(a,n);
    printf("转换后的数组为\n");
    for(i=0;i<N;i++)
    {
        for(j=0;j<N;j++)
        {
            printf("%4d",a[i][j]);
        }
        printf("\n");
    }
}
